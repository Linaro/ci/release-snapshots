#!/bin/bash
set -e
export RELEASE=21.08
export OE_RELEASE=411
export D_RELEASE=537
export R_RELEASE=83
export DRYRUN="--dryrun"
export RELEASES=s3://publishing-ie-linaro-org/releases
export SNAPSHOTS=s3://publishing-ie-linaro-org/snapshots

aws s3 rm --recursive $DRYRUN $RELEASES/96boards/dragonboard845c/linaro/debian/latest
aws s3 rm --recursive $DRYRUN $RELEASES/96boards/dragonboard845c/linaro/openembedded/latest
aws s3 rm --recursive $DRYRUN $RELEASES/96boards/dragonboard845c/linaro/rescue/latest

echo releases/96boards/dragonboard845c/linaro/openembedded/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/dragonboard845c/linaro/openembedded/latest/.s3_linked_from
echo releases/96boards/dragonboard845c/linaro/debian/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/dragonboard845c/linaro/debian/latest/.s3_linked_from
echo releases/96boards/dragonboard845c/linaro/rescue/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/dragonboard845c/linaro/rescue/latest/.s3_linked_from

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/dragonboard845c/linaro/debian/${D_RELEASE}/ \
    $RELEASES/96boards/dragonboard845c/linaro/debian/$RELEASE

aws s3 cp $DRYRUN lt-docs/debian/boards/dragonboard845c/README.textile \
    $RELEASES/96boards/dragonboard845c/linaro/debian/$RELEASE/README.textile

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/dragonboard845c/linaro/openembedded/dunfell/${OE_RELEASE}/ \
    $RELEASES/96boards/dragonboard845c/linaro/openembedded/$RELEASE

aws s3 cp $DRYRUN lt-docs/openembedded/boards/dragonboard845c/README.textile \
    $RELEASES/96boards/dragonboard845c/linaro/openembedded/$RELEASE/README.textile

aws s3 cp $DRYRUN lt-docs/openembedded/boards/dragonboard845c/testplan.html \
    $RELEASES/96boards/dragonboard845c/linaro/openembedded/$RELEASE/rpb/testplan.html
aws s3 cp $DRYRUN lt-docs/openembedded/boards/dragonboard845c/testreport.html \
    $RELEASES/96boards/dragonboard845c/linaro/openembedded/$RELEASE/rpb/testreport.html

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/dragonboard845c/linaro/rescue/${R_RELEASE}/ \
    $RELEASES/96boards/dragonboard845c/linaro/rescue/$RELEASE
