#!/bin/bash
set -e
export RELEASE=21.04.1
export OE_RELEASE=10
export DRYRUN="--dryrun"
export RELEASES=s3://publishing-ie-linaro-org/releases
export SNAPSHOTS=s3://publishing-ie-linaro-org/snapshots

aws s3 rm --recursive $DRYRUN $RELEASES/96boards/rb5/linaro/rescue/latest
echo releases/96boards/rb5/linaro/rescue/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/rb5/linaro/rescue/latest/.s3_linked_from

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/qrb5165-rb5/linaro/rescue/${OE_RELEASE}/ \
    $RELEASES/96boards/rb5/linaro/rescue/$RELEASE

