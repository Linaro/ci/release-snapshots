!/bin/bash
set -e
export RELEASE=21.08
export OE_RELEASE=411
export D_RELEASE=115
export R_RELEASE=14
export DRYRUN="--dryrun"
export RELEASES=s3://publishing-ie-linaro-org/releases
export SNAPSHOTS=s3://publishing-ie-linaro-org/snapshots

aws s3 rm --recursive $DRYRUN $RELEASES/96boards/rb5/linaro/openembedded/latest
aws s3 rm --recursive $DRYRUN $RELEASES/96boards/rb5/linaro/debian/latest
echo releases/96boards/rb5/linaro/openembedded/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/rb5/linaro/openembedded/latest/.s3_linked_from
echo releases/96boards/rb5/linaro/debian/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/rb5/linaro/debian/latest/.s3_linked_from

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/qrb5165-rb5/linaro/openembedded/dunfell/${OE_RELEASE}/ \
    $RELEASES/96boards/rb5/linaro/openembedded/$RELEASE
aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/qrb5165-rb5/linaro/debian/${D_RELEASE}/ \
    $RELEASES/96boards/rb5/linaro/debian/$RELEASE

aws s3 cp $DRYRUN lt-docs/debian/boards/rb5/README.textile \
    $RELEASES/96boards/rb5/linaro/debian/$RELEASE/README.textile
aws s3 cp $DRYRUN lt-docs/openembedded/boards/rb5/README.textile \
    $RELEASES/96boards/rb5/linaro/openembedded/$RELEASE/README.textile
aws s3 cp $DRYRUN lt-docs/openembedded/boards/rb5/testplan.html \
    $RELEASES/96boards/rb5/linaro/openembedded/$RELEASE/rpb/testplan.html
aws s3 cp $DRYRUN lt-docs/openembedded/boards/rb5/testreport.html \
    $RELEASES/96boards/rb5/linaro/openembedded/$RELEASE/rpb/testreport.html

aws s3 rm --recursive $DRYRUN $RELEASES/96boards/rb5/linaro/rescue/latest
echo releases/96boards/rb5/linaro/rescue/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/96boards/rb5/linaro/rescue/latest/.s3_linked_from

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/96boards/qrb5165-rb5/linaro/rescue/${R_RELEASE}/ \
    $RELEASES/96boards/rb5/linaro/rescue/$RELEASE

