#!/bin/bash
set -e
export RELEASE=21.10
export OE_RELEASE=478
export DRYRUN="--dryrun"
export RELEASES=s3://publishing-ie-linaro-org/releases
export SNAPSHOTS=s3://publishing-ie-linaro-org/snapshots

aws s3 rm --recursive $DRYRUN $RELEASES/members/qualcomm/openembedded/sa8155p-adp/latest
echo releases/members/qualcomm/openembedded/sa8155p-adp/$RELEASE > .s3_linked_from
aws s3 cp $DRYRUN .s3_linked_from $RELEASES/members/qualcomm/openembedded/sa8155p-adp/latest/.s3_linked_from

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/member-builds/qcomlt/boards/sa8155p-adp/openembedded/dunfell/${OE_RELEASE}/ \
    $RELEASES/members/qualcomm/openembedded/sa8155p-adp/$RELEASE

aws s3 cp $DRYRUN lt-docs/openembedded/boards/sa8155p-adp/README.textile \
    $RELEASES/members/qualcomm/openembedded/sa8155p-adp/$RELEASE/README.textile

# Delete files not to be released

aws s3 rm $DRYRUN s3://publishing-ie-linaro-org/releases/members/qualcomm/openembedded/sa8155p-adp/21.10/rpb/boot-initramfs-kerneltest-full-image-sa8155p-adp--5.15-r0-sa8155p-adp-20211027192435-478.img
aws s3 rm $DRYRUN s3://publishing-ie-linaro-org/releases/members/qualcomm/openembedded/sa8155p-adp/21.10/rpb-wayland/initramfs-kerneltest-full-image-sa8155p-adp-20211027183248-478.rootfs.*

