#!/bin/bash
set -ex
export TARGET=2020.09.dunfell.2
export SNAP_54=431
export SNAP_419=345
export DRYRUN="--dryrun"
export RELEASES=s3://publishing-ie-linaro-org/releases
export SNAPSHOTS=s3://publishing-ie-linaro-org/snapshots

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/openembedded/schneider/linaro-dunfell-4.19/soca9/$SNAP_419/dip/ \
    $RELEASES/members/schneider/openembedded/$TARGET/soca9-4.19/

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/openembedded/schneider/linaro-dunfell-4.19/rzn1d/$SNAP_419/dip/ \
    $RELEASES/members/schneider/openembedded/$TARGET/rzn1d-4.19/

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/openembedded/schneider/linaro-dunfell-5.4/soca9/$SNAP_54/dip/ \
    $RELEASES/members/schneider/openembedded/$TARGET/soca9-5.4/

aws s3 cp $DRYRUN --recursive $SNAPSHOTS/openembedded/schneider/linaro-dunfell-5.4/rzn1d/$SNAP_54/dip/ \
    $RELEASES/members/schneider/openembedded/$TARGET/rzn1d-5.4/

aws s3 cp $DRYRUN reflash-lces2-2020.09-dunfell.2.tar.bz2 $RELEASES/members/schneider/openembedded/$TARGET/rzn1d-4.19/
aws s3 cp $DRYRUN reflash-lces2-2020.09-dunfell.2.tar.bz2 $RELEASES/members/schneider/openembedded/$TARGET/rzn1d-5.4/


